const gulp = require('gulp')
const less = require('gulp-less')
const cleanCss = require('gulp-clean-css')
const sourcemaps = require('gulp-sourcemaps')
const concat = require('gulp-concat')
const uglify = require('gulp-uglify')

const filePath = {
  styles: {
    src: 'src/less/*.less',
    dest: 'pub/css'
  },
  scripts: {
    src: 'src/js/*.js',
    dest: 'pub/js'
  },
  vendor: [
    'bower_components/Font-Awesome/svg-with-js/js/fontawesome-all.min.js'
  ]
}

gulp.task('less', ()=> {
  gulp.src(filePath.styles.src)
    .pipe(sourcemaps.init())
    .pipe(less())
    .pipe(cleanCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(filePath.styles.dest))
});

gulp.task('vendor', ()=> {
  gulp.src(filePath.vendor)
    .pipe(concat('vendor.js'))
    .pipe(uglify())
    .pipe(gulp.dest(filePath.scripts.dest))
})

gulp.task('watch', ()=>{
  gulp.watch(filePath.styles.src, ['less'])
})

gulp.task('default',['watch','vendor'])