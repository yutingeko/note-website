const express = require('express')
const path = require('path')
const app = express()

app.set('view engine', 'pug')
app.use(express.static('pub'))
app.get('/', (req, res) => {
  res.render('index')
  // res.sendFile(path.join(__dirname, 'view', 'index.html'));
})

let server = app.listen(process.env.PORT || 3000, ()=> {
  let port = server.address().port;
  console.log("App now running on port", port);
});
